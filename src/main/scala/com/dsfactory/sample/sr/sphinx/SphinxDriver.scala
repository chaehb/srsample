package com.dsfactory.sample.sr.sphinx

import java.io.InputStream

import edu.cmu.sphinx.api._

/**
  * Created by chaehb on 08/08/2016.
  */
object SphinxDriver {
  val ACOUSTIC_MODEL_PATH = "data/sphinx/en_US/en-us"
  val DICTIONARY_PATH = "data/sphinx/en_US/cmudict-en-us.dict"
  val LANGUAGE_MODEL_PATH = "data/sphinx/en_US/en-us.lm.bin"

  var _configuration:Configuration = new Configuration

  def configuration = {
    if(_configuration.getAcousticModelPath == null) {
      _configuration.setAcousticModelPath(ACOUSTIC_MODEL_PATH)
      _configuration.setDictionaryPath(DICTIONARY_PATH)
      _configuration.setLanguageModelPath(LANGUAGE_MODEL_PATH)
    }

    _configuration
  }

  def liveSpeechRecognizer = {
    new LiveSpeechRecognizer(configuration)
  }

  def streamSpeechRecognizer(inputStream:InputStream):SpeechResult = {
    val recognizer = new StreamSpeechRecognizer(configuration)
    recognizer.startRecognition(inputStream)
    val result = recognizer.getResult
    recognizer.stopRecognition

    result
  }
}
