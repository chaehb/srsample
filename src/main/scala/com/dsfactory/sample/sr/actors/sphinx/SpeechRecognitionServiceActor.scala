package com.dsfactory.sample.sr.actors.sphinx

import akka.actor.{Actor, ActorLogging, Props}
import akka.actor.Actor.Receive
import akka.event.Logging

/**
  * Created by chaehb on 08/08/2016.
  */
class SpeechRecognitionServiceActor extends Actor with ActorLogging {
  override val log = Logging(context.system,this)

  override def receive: Receive = {
    case _:String => println("Hello SRS")
  }
}

object SpeechRecognitionServiceActor {
  def props = Props[SpeechRecognitionServiceActor]
}
