package com.dsfactory.sample.sr.actors

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.event.Logging
import akka.japi.pf.DeciderBuilder
import com.dsfactory.sample.sr.actors.kafka.KafkaServiceActor
import com.dsfactory.sample.sr.actors.sphinx.SpeechRecognitionServiceActor

/**
  * Created by chaehb on 08/08/2016.
  */
class MainSupervisor extends Actor with ActorLogging {
  import akka.actor.OneForOneStrategy
  import akka.actor.SupervisorStrategy._
  import scala.concurrent.duration._

  override val log = Logging(context.system,this)

  var workers = Map[ActorRef,ActorRef]()

  override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 10,withinTimeRange = 1 minute){
    case _: ArithmeticException      => Resume
    case _: NullPointerException     => Restart
    case _: IllegalArgumentException => Stop
    case _: Exception                => Escalate
  }

  override def preStart(): Unit = {
    super.preStart()

    println("Supervisor started")
  }

  override def receive: Receive = {
    case "SR" => {
      val actor = context.actorOf(SpeechRecognitionServiceActor.props)
      actor ! "SR"
    }
    case "Kafka" => {
      val actor = context.actorOf(KafkaServiceActor.props)
      actor ! "Kafka"
    }
    case p: Props => {
      sender() ! context.actorOf(p)
    }
  }
}

object MainSupervisor {
  val props = Props[MainSupervisor]
//  val props = Props(classOf[MainSupervisor],"arg")
}
