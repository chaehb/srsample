package com.dsfactory.sample.sr.actors.kafka

import akka.actor.{Actor, ActorLogging, Props}
import akka.event.Logging

/**
  * Created by chaehb on 08/08/2016.
  */
class KafkaServiceActor extends Actor with ActorLogging{
  override val log = Logging(context.system,this)

  override def receive: Receive = {
    case _:String => {
      println("Hello kafka")
    }
  }
}

object KafkaServiceActor {
  def props = Props[KafkaServiceActor]
}
