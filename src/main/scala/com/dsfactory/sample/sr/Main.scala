package com.dsfactory.sample.sr

import java.io.{File, FileInputStream}
import javax.sound.sampled.AudioSystem

import akka.actor.{ActorRef, ActorSystem, Props}
import com.dsfactory.sample.sr.actors.MainSupervisor
import com.dsfactory.sample.sr.actors.kafka.KafkaServiceActor
import com.dsfactory.sample.sr.actors.sphinx.SpeechRecognitionServiceActor
import com.dsfactory.sample.sr.sphinx.SphinxDriver

import scala.collection.JavaConversions._

/**
  * Created by chaehb on 08/08/2016.
  */
object Main {
  def main(args: Array[String]): Unit = {
//    val system = ActorSystem.create("speech-recognition-system")
//
//    val srService = system.actorOf(Props.create(classOf[MainSupervisor]),"main-supervisor")
//
//    println("========")
//    for(i <- 1 to 10){
//      srService ! "SR"
//      srService ! "Kafka"
//    }
//    println("=======")

//    val recognizer = SphinxDriver.streamSpeechRecognizer(new FileInputStream("/Users/chaehb/Desktop/test.wav"))
//
//    recognizer.getWords.foreach(word =>{
//      println(word)
//    })

    val mp3 = AudioSystem.getAudioInputStream(new File("/Users/chaehb/Desktop/test.wav"))

//    val recognizer2 = SphinxDriver.streamSpeechRecognizer(new FileInputStream("/Users/chaehb/Desktop/Example.ogg"))
    val recognizer2 = SphinxDriver.streamSpeechRecognizer(mp3)

    recognizer2.getWords.foreach(word =>{
      println(word)
    })

  }
}
