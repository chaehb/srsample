import NativePackagerHelper._

name := """SRSample"""

version := "1.0.0"

scalaVersion := "2.11.8"

libraryDependencies ++= {
	val V_SPARK = "2.0.0"
	val V_KAFKA = "0.10.0.0"
	val V_ZOOKEEPER = "3.4.8"
	val V_CMU_SPHINX = "5prealpha-SNAPSHOT"
	val V_PHANTOM = "1.27.0"
	val V_AKKA = "2.4.9-RC2"
	val V_S3 = "1.11.21"
	val V_SOLRJ = "6.1.0"

	Seq(
		//latest akka
		"com.typesafe.akka" %% "akka-actor" % V_AKKA,
		"com.typesafe.akka" %% "akka-slf4j" % V_AKKA,
		"com.typesafe.akka" %% "akka-stream" % V_AKKA,
		// apache kafka
		"org.apache.kafka" % "kafka-clients" % V_KAFKA,
		// zookeeper
		"org.apache.zookeeper" % "zookeeper" % V_ZOOKEEPER,
		// Phantom : cassandra client driver
		"com.websudos" %% "phantom-dsl" % V_PHANTOM,
		"com.websudos" %% "phantom-reactivestreams" % V_PHANTOM,
		"org.apache.solr" % "solr-solrj" % V_SOLRJ,
		// amazonaws-s3-sdk for leofs
		"com.amazonaws" % "aws-java-sdk" % V_S3,
		// CMU sphinx
		"edu.cmu.sphinx" % "sphinx4-core" % V_CMU_SPHINX,
		"edu.cmu.sphinx" % "sphinx4-data" % V_CMU_SPHINX,
		// mp3,ogg
		"com.googlecode.soundlibs" % "mp3spi" % "1.9.5.4",
		"com.googlecode.soundlibs" % "vorbisspi" % "1.0.3.3",
    "com.googlecode.soundlibs" % "tritonus-all" % "0.3.7.2"
	)
}


resolvers ++= Seq(
	"scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
	Resolver.sonatypeRepo("snapshots"),
	Resolver.sonatypeRepo("releases"),
	Resolver.bintrayRepo("websudos", "oss-releases"),
	"scala-tools" at "https://oss.sonatype.org/content/groups/scala-tools/"
)

lazy val root = (project in file(".")).enablePlugins(JavaServerAppPackaging)

mainClass in Compile := Some("com.dsfactory.sample.sr.Main")

mappings in Universal ++= {
  // optional example illustrating how to copy additional directory
  directory("scripts") ++
  // copy configuration files to config directory
  contentOf("src/main/resources").toMap.mapValues("config/" + _)
}

// add 'config' directory first in the classpath of the start script,
// an alternative is to set the config file locations via CLI parameters
// when starting the application
scriptClasspath := Seq("../config/") ++ scriptClasspath.value

licenses := Seq(("CC0", url("http://creativecommons.org/publicdomain/zero/1.0")))

fork in run := true

javaOptions in run ++= Seq(
	"-Xms1G",
	"-Xmx4G",
	"-server",
	"-XX:-UseGCOverheadLimit",
	"-XX:+UseParallelGC"
)
